# Step X1: Add a cloud provider with credentials set using AWS CLI
provider "aws" {
    profile = "default"
    region = "us-east-2"
 # Unless specified here explicitly, the provider will use the default profile and region
}

/*
# Step X2:  Creating variables to be referenced by the resources below
variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}	
# To be used by subnets; avail_zone must be within the VPC region only.
variable env_prefix {}
# we use this var to predefine the subnet region at vars file.
# vpc is built on the default region set on the AWS configure file
# We can also set the region at the providers section
variable my_ip {}
*/